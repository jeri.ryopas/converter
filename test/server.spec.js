const expect = require('chai').expect;
const request = require('request');
const app = require("../src/server");
const port = 3000;

describe("Color code converter API", () => {
    let server = undefined; // toimii ilman tätäkii :)
    before("Start server", (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening on localhost:${port}`);
            done();
        })
    })

    describe("RGB to Hex conversion", () => {
        const url = `http://localhost:${port}/rgb-to-hex?red=255&green=255&blue=255`;
        it("returns status code 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            })
        })
        it("returns the color in hex", (done) => {
            request(url, (error, response, body) => {
                expect(body).to.equal("ffffff");
                done();
            })
        })
    })
    describe("Hex to RGB conversion", () => {
        const urli = `http://localhost:${port}/hex-to-rgb?hexa=%23ffffff`;
        it("returns status code 200", (done) => {
            request(urli, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            })
        })
        it("returns the color in rgb", (done) => {
            request(urli, (error, response, body) => {
                expect(body).to.equal("255,255,255");
                done();
            })
        })

        after("Close server", (done) => {
            server.close();
            done()
        })
    })
})