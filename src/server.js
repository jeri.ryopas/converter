const express = require('express');
const app = express();
const converter = require("./converter");
const port = 3000;

// endpoint localhost:3000
app.get('/', (req, res) => res.send("Hello"));

// endpoint localhost:3000/rgb-to-hex?red=255&green=0&blue=0
app.get('/rgb-to-hex', (req,res) =>{
    const red = parseInt(req.query.red, 10)
    const green = parseInt(req.query.green, 10)
    const blue = parseInt(req.query.blue, 10)
    const hex = converter.rgbToHex(red,green,blue);
    res.send(hex)
})

// endpoint localhost:3000/hex-to-rgb?hexa=%23ff0000
app.get('/hex-to-rgb', (req,res) => {
    const hexa = req.query.hexa
    const rgbConv = converter.hexToRgb(hexa)
    res.send(rgbConv)
})

if (process.env.NODE_ENV === 'test') {
    module.exports = app;
} else {
    app.listen(port, () => console.log(`server listening on localhost:${port}`))
}
