// TDD - Test Driven Decelopment - Unit testing

const expect = require('chai').expect;
const converter = require('../src/converter');

describe("Color code converter",() => {
    describe("RGB to Hex conversion", () => {
        it("converts the basic colors",()=>{
            const redHex = converter.rgbToHex(255,0,0); // #ff0000
            const greenHex = converter.rgbToHex(0,255,0); // #00ff00
            const blueHex = converter.rgbToHex(0,0,255); // #0000ff

            expect(redHex).to.equal("ff0000")
            expect(greenHex).to.equal("00ff00")
            expect(blueHex).to.equal("0000ff")
        })
    })
    describe("Hex to RGB",() => {
        it("converts the basic colors", ()=>{
            const redCol = converter.hexToRgb("#ff0000");
            const greenCol = converter.hexToRgb("#00ff00");
            const blueCol = converter.hexToRgb("#0000ff");

            expect(redCol).to.equal("255,0,0")
            expect(greenCol).to.equal("0,255,0")
            expect(blueCol).to.equal("0,0,255")

        })
    })
})