const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex);
}

module.exports = {
    rgbToHex: (r, g, b) => {
        const redHex = r.toString(16); // ff
        const greenHex = g.toString(16);
        const blueHex = b.toString(16);

        return pad(redHex) + pad(greenHex) + pad(blueHex); // "ff0000"
    },
    hexToRgb: (hex) => {
        const normal = hex.match(/^#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i);
        if (normal) return normal.slice(1).map(e => parseInt(e, 16)).toString();

        const shorthand = hex.match(/^#([0-9a-f])([0-9a-f])([0-9a-f])$/i);
        if (shorthand) return shorthand.slice(1).map(e => 0x11 * parseInt(e, 16)).toString();

        return null;
    }
}